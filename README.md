# Hammerkit Typescript Restore Demo

This repo is short demo how the store/restore commands can be used, to improve the CI performance. 
This demo is a very tiny project to illustrate the setup and therefore the performance improvements are not that representive as it can be in bigger projects.
